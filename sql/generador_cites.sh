#!/bin/bash
MAX=80000000;

if test "$#" -ne 1; then
    echo "No has passat els paràmetres correctes, en cal 1, el número de matrícules que vols";

else

  if test "$1" -gt $MAX; then
    echo "No poden haver-hi tantes matrícules, màxim 80000000!"
  else

    if test "$1" -ge 10000; then
      echo "El procés pot trigar una mica, paciència..."
      sleep 5;
    fi

    # Fitxer on van els inserts del script
    fitxer="insert_$1.sql";
	  echo "-- Script auto generació amb BASH, creació script SQL per insertar $1 registres" > $fitxer;

    #Les matrícules només poden tenir consonants, menys Q y Ñ
    consonantes=(B C D F G H J K L M N P R S T V W X Y Z);
    count="$1";
    repeatCita=0;
	DATE=$(date '+%Y-%m-%d %H:%M:%S');
	# El Script genera les cites a partir del dia següent a avuí
	INICIO=$(date '+%Y-%m-%d 07:30:00' -d "$DATE 1 day");

    #Creem una variable amb la direcció de les paraules del diccionario del sistema
    palabras="words"
    tL=`awk 'NF!=0 {++c} END {print c}' $palabras`;

    #Bucle per a tots els números de 0 a 9999 i totes les lletres de BBB a ZZZ
    for mat in $(seq 0 9999) #
    do
      for i in ${consonantes[@]}
      do
        for j in ${consonantes[@]}
        do
          for k in ${consonantes[@]}
          do
            # Generem de l'index $mat un total de 4 digits
			number=`expr $mat | awk '{ printf "%04g\n", $0 }'`;

      #Amb un número aleatori fem que es pugin repetir dates i aleshores
      if test $repeatCita -eq 1; then
          repeatCita=0;
      else
        repeatCita=$(( RANDOM%2 ));
      fi

			#Agafem una paraula del diccionario de forma aleatoria
			# I d'aquestes li treiem els single quote per evitar problemes a la base de dades
			rnum=$((RANDOM%$tL+1))
			gennombre=`sed -n "$rnum p" $palabras`;
			nombre=$(echo $gennombre | sed "s/'//g");
			rnum=$((RANDOM%$tL+1))
			genapellido=`sed -n "$rnum p" $palabras`;
			apellido=$(echo $genapellido | sed "s/'//g");
			nombreCorreo=`echo "$nombre" | tr '[:upper:]' '[:lower:]'`;
			apellidoCorreo=`echo "$apellido" | tr '[:upper:]' '[:lower:]'`;

      if test $repeatCita -ne 1; then
			# Es genera un número aleatori, simplement per deixar 2 forats a una hora en comptes d'un.
        aleatori=$(( RANDOM%2 ));
  			if test $aleatori -eq 1; then
  				INICIO=$(date '+%Y-%m-%d %H:%M:%S' -d "$INICIO 60 minute");
  			else
  				INICIO=$(date '+%Y-%m-%d %H:%M:%S' -d "$INICIO 30 minute");
  			fi
      fi

			# Si son les 20 hores passa al día següent
			HORA=$(date '+%H' -d "$INICIO");
			if test $HORA -ge 20; then
				INICIO=$(date '+%Y-%m-%d 08:00:00' -d "$INICIO 1 day");
			fi

			# Nomes es generen cites de dilluns a divendres
			diaSetmana=$(date '+%u' -d "$INICIO");
			if test $diaSetmana -eq 6; then
				INICIO=$(date '+%Y-%m-%d 08:00:00' -d "$INICIO 2 day");
			elif test $diaSetmana -eq 7; then
				INICIO=$(date '+%Y-%m-%d 08:00:00' -d "$INICIO 1 day");
			fi
			telefono=$(date '+%N');
			#Generem aleshores l'INSERT per a la base de dades
			echo "insert into motorameda.cites(matricula,nom,cognom,telefon,correu,data_cita) values (\"$number$i$j$k\",\"$nombre\",\"$apellido\",\"$telefono\",\"$nombreCorreo.$apellidoCorreo@gmail.com\",\"$INICIO\");" >> $fitxer;
			echo "Falten $count inserts per generar";
            count=`expr $count - 1`;

            # Si arribem al número de matrícules que vol l'usuari parem i sortim
            if test "$count" -eq 0; then
              exit;
            fi
          done
        done
      done
    done
  fi
fi
