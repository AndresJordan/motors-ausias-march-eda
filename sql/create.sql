/* Creación de la base de datos */
CREATE DATABASE IF NOT EXISTS motorameda;
/* Creación de la tabla cites */
CREATE TABLE IF NOT EXISTS motorameda.cites (
`id` INT NOT NULL AUTO_INCREMENT,
`matricula` VARCHAR(7) NOT NULL,
`nom` VARCHAR(128) NOT NULL,
`cognom` VARCHAR(128) NOT NULL,
`telefon` VARCHAR(9) NOT NULL,
`correu` VARCHAR(128) NOT NULL,
`data_cita` DATETIME NOT NULL,
`data_reserva`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (ID)
) ENGINE = InnoDB DEFAULT CHARSET=utf8
COMMENT = 'Taula que conté totes les cites del taller MOTORAMEDA';
/* Creación de la tabla admin */
CREATE TABLE IF NOT EXISTS motorameda.admin (
`desactivar_web` BOOLEAN NOT NULL,
`backup_auto` BOOLEAN NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET=utf8
COMMENT = 'Taula que conté totes els paràmetres de configuració de l\' admin per el taller MOTORAMEDA';
/* Insertamos por defecto los valores de la tabla admin */
INSERT INTO `admin`(`desactivar_web`, `backup_auto`) VALUES (0,0)
