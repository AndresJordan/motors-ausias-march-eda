<?php
// Array que contiene los usuarios y contraseñas validos
$valid_passwords = array ($userAdmin => $passAdmin);
// Obtenemos los usuarios de la array anterior
$valid_users = array_keys($valid_passwords);
// Asignamos el usuario y contraseña de autentificación
$user = $_SERVER['PHP_AUTH_USER'];
$pass = $_SERVER['PHP_AUTH_PW'];
// Comprobamos si es válido el usuario y contraseña
$validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);
// En caso que no lo sea le solicitamos el usuario y contraseña
if (!$validated) {
  header('WWW-Authenticate: Basic realm="Motorameda autentificación"');
  header('HTTP/1.0 401 Unauthorized');
  die ("<h2>Usuari o contrasenya incorrectes.</h2>");
}
?>