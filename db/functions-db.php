<?php
// Declaramos los campos requeridos como variable global
$idCita = 'id';
$campoMatricula = 'matricula';
$campoDataCita = 'data_cita';
$campoDataReserva = 'data_reserva';
$camposRequeridos = array('nom','cognom','telefon','correu');
// Datos de conexion a la base de datos
$campoDesactivarWeb = 'desactivar_web';
$campoBackupAuto = 'backup_auto';
// Nombre de la tabla en la base de datos
$db_table = 'cites';
$db_table_admin = 'admin';
$db_table_historic = "historic";
// Conexión a la base de datos
$db_server;

// Función que usaremos para la conexion a la base de datos
function obrirConexioDB(){
		global $db_server, $db_hostname, $db_username, $db_password;
		$db_server = mysqli_connect($db_hostname, $db_username, $db_password);
	  if (!$db_server){
			return false;
		}
		return true;
}

// Función que usaremos para la cerrar la conexion a la base de datos
function tancarConexioDB(){
	global $db_server;
	mysqli_close($db_server);
}

//Funció per esborrar les taules
function dropTables(){
	global $db_table_historic,$idCita,$campoMatricula,$campoDataCita,$campoDataReserva,$camposRequeridos,$campoDesactivarWeb,$campoBackupAuto,$db_database,$db_table,$db_table_admin,$db_server;

		if (mysqli_select_db($db_server, $db_database)) {
			// Taula de cites/reserves
			$query="DROP TABLE $db_database.$db_table";
			//Realitzar consulta a la base de dades
			$result = mysqli_query($db_server, $query);
			mysqli_free_result($result);

			// Taula paràmetres Admin
			$query="DROP TABLE $db_database.$db_table_admin";
			//Realitzar consulta a la base de dades
			$result = mysqli_query($db_server, $query);
			//Comprovar si la consulta es vuida
			$rows = mysqli_num_rows($result);
			mysqli_free_result($result);
			return true;
		}
		return false;
}

//Funció per a la creació de la base de dades necessaries
function createTables(){
	global $idCita,$campoMatricula,$campoDataCita,$campoDataReserva,$camposRequeridos,$campoDesactivarWeb,$campoBackupAuto,$db_database,$db_table,$db_table_admin,$db_server;

		if (mysqli_select_db($db_server, $db_database)) {
			// Taula de cites/reserves
			$query="CREATE TABLE IF NOT EXISTS $db_database.$db_table (
			$idCita INT NOT NULL AUTO_INCREMENT,
			$campoMatricula VARCHAR(7) NOT NULL,
			$camposRequeridos[0] VARCHAR(128) NOT NULL,
			$camposRequeridos[1] VARCHAR(128) NOT NULL,
			$camposRequeridos[2] VARCHAR(9) NOT NULL,
			$camposRequeridos[3] VARCHAR(128) NOT NULL,
			$campoDataCita DATETIME NOT NULL,
			$campoDataReserva  TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			PRIMARY KEY ($idCita)
			) ENGINE = InnoDB DEFAULT CHARSET=utf8
			COMMENT = 'Taula que conté totes les cites del taller MOTORAMEDA'";

			//Realitzar consulta a la base de dades
			$result = mysqli_query($db_server, $query);
			mysqli_free_result($result);
			// Taula per a paràmetres Admin
			$query="CREATE TABLE IF NOT EXISTS $db_database.$db_table_admin (
			$campoDesactivarWeb BOOLEAN NOT NULL,
			$campoBackupAuto BOOLEAN NOT NULL
			) ENGINE = InnoDB DEFAULT CHARSET=utf8
			COMMENT = 'Taula que conté totes els paràmetres de configuració de l\' admin per el taller MOTORAMEDA'";

			//Realitzar consulta a la base de dades
			$result = mysqli_query($db_server, $query);
			//Comprovar si la consulta es vuida
			$rows = mysqli_num_rows($result);
			mysqli_free_result($result);

			// Insert per a la taula admin, necessari
			$query="INSERT INTO $db_database.$db_table_admin VALUES (0,0)";
			//Realitzar consulta a la base de dades
			$result = mysqli_query($db_server, $query);
			//Comprovar si la consulta es vuida
			$rows = mysqli_num_rows($result);
			mysqli_free_result($result);

			$query="CREATE TABLE IF NOT EXISTS $db_database.$db_table_historic (
			$idCita INT NOT NULL,
			$campoMatricula VARCHAR(7) NOT NULL,
			$camposRequeridos[0] VARCHAR(128) NOT NULL,
			$camposRequeridos[1] VARCHAR(128) NOT NULL,
			$camposRequeridos[2] VARCHAR(9) NOT NULL,
			$camposRequeridos[3] VARCHAR(128) NOT NULL,
			$campoDataCita DATETIME NOT NULL,
			$campoDataReserva  TIMESTAMP
			) ENGINE = InnoDB DEFAULT CHARSET=utf8
			COMMENT = 'Taula que conté historics de cites'";
			//Realitzar consulta a la base de dades
			$result = mysqli_query($db_server, $query);
			//Comprovar si la consulta es vuida
			$rows = mysqli_num_rows($result);
			mysqli_free_result($result);
			return true;
	}
	return false;
}
?>
