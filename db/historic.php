<?php
// Importem les dades per connectar-se a la BD
require_once "connect-db.php";

// Obrim connexió
obrirConexioDB();

// Si la connexió es correcte, executem les query
if (mysqli_select_db($db_server, $db_database)) {
  // Query que fa el insert a historic
  $query = "INSERT INTO $db_database.$db_table_historic SELECT * FROM $db_database.$db_table WHERE DATE_FORMAT($campoDataCita,'%Y-%m-%d') <= DATE_FORMAT(NOW(),'%Y-%m-%d')";
  $result = mysqli_query($db_server, $query);
  if($result){
  // Delete un cop finalitza l'insert
  $query = "DELETE FROM $db_database.$db_table  WHERE DATE_FORMAT($campoDataCita,'%Y-%m-%d') <= DATE_FORMAT(NOW(),'%Y-%m-%d')";
  $result = mysqli_query($db_server, $query);
  }
}else{

  die('Invalid connect : ' . mysqli_error($db_server));

}

mysqli_free_result($result);
tancarConexioDB();

?>
