# MOTORAMEDA

# Motor Ausiàs March EDA

​Aplicació​ ​ de​ ​ gestió​ de​ ​ cites​ ​ per​ ​ un​ ​ 1​ ​ centre​ ​ d'Inspecció​ ​ tècnica​ ​ de vehícles​ ​ (ITV)​ ​ per a​ ​ vehícles​ ​ d'una​ ​ tipologia​ ​ (turismes).

## Full de seguiment ##

[Full de càlcul del seguiment](https://docs.google.com/spreadsheets/d/1UlI6VV44kwz6DMilmYSsfLYo3ejZZwlWYg8EjeFE_PI/edit?usp=sharing)

## Directoris ##

/admin - Configuració del panell d'administració

/db - Configuració de la base de dades

/doc - Informació i manuals d'usuari

/public_html - Fitxers web

/sql - Fitxers script per a la BD

/tests - Fitxers de test de la web amb JMeter

## Prototip ##

[Moqup](https://app.moqups.com/a15eduvalcal@iam.cat/lTksWIxsqz/view)

## LABS ##

[LABS](http://labs.iam.cat/~a17edwjorzam/)

## Manual d'instalació ##

[Manual](https://bitbucket.org/AndresJordan/motors-ausias-march-eda/wiki/Home)

## Futures millores ##

Ampliar la tipoogia de vehicles.

Ampliar d'un centre a diversos centres.

Informe mensual d'administració.

## Autors ##

- Eduard Vallès
- Daniel Martín
- Andrés Jordán
 
