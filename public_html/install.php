<?php
// Fichero con los datos para la conexión a la base de datos
require_once "../db/connect-db.php";
// Obrim la connexió a la base de dades
if (obrirConexioDB()) {
	echo "<h3>Conexión establecida.</h3><br/>";
	// Borramos las tablas que habían
	if (dropTables()) {
		echo "<h3>Eliminación de las tablas correcto.</h3><br/>";
		// Creamos vacias las tablas
		if (createTables()) {
			echo "<h3>Creación de las tablas correcto.</h3><br/>";
			// Mostramos el mensaje correspondiente
			echo "<h2>Instalación ejecutada correctamente.</h2>";
		} else {
			echo "<h3>No se han podido crear las tablas.</h3><br/>";
		}
	} else {
		echo "<h3>No se han podido eliminar las tablas.</h3><br/>";
	}
} else {
	echo "<h3>No se ha podido establecer la conexión.</h3><br/>";
}
// Cerramos la conexión con la base de datos
tancarConexioDB();
?>