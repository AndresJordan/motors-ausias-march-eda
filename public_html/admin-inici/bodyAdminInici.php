<?php
// Fichero con los datos para la conexión como usuario administrador
require_once('../../admin/connect-admin.php');
// Cargamos los estilos necesarios para la web de bodyAdminInici.php
$htmlstrhead .= '<link rel="stylesheet" href="css/styleAdminInici.css">';
// Cargamos el código en el body de la web de bodyAdminInici.php
$htmlstrbdy .= "<article class='container'>"
							."<h2>Menú Administració</h2>"
							."<h3>Opcions disponibles:</h3>"
							."<ul>"
								."<li><a href='../admin-llista/bodyAdminLlista.php'>Llista</a></li>"
								."<li><a href='../admin-estadistica/bodyAdminEstadistica.php'>Estadístiques</a></li>"
								."<li><a href='../admin-manteniment/bodyAdminManteniment.php'>Manteniment</a></li>"
								."<li><a href='../admin-tancar-sessio/bodyAdminTancarSessio.php'>Tancar Sessió</a></li>"
							."<ul>"
							."</article>";
// Cargamos la plantilla de la web
require_once('../PlantillaAdmin.php');
?>