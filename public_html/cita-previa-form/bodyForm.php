<?php
	// Cargamos el php necesario para los datos
	require_once('php/funciones.php');
	// Cargamos los estilos necesarios para la web de bodyForm.php
	$htmlstrhead = '<link rel="stylesheet" href="css/styleForm.css">';
	// Cargamos el código en el body de la web de bodyForm.php
	$htmlstrbdy ="<article class='matricula'>"
		."<h1>Matrícula</h1>"
		."$matricula"
	."</article>"
	."<article class='dades'>"
    ."<form method='POST' action='bodyForm.php'>"
		."<label>Nom: </label><input type='text' name='nom' placeholder='MotorAmeda' pattern='^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð]{2,48}$' required/>"
    ."<label>Cognom: </label><input type='text' name='cognom' placeholder='Ausias March' pattern='^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð]{2,48}$' required/>"
		."<label>Correu: </label><input type='email' name='email' placeholder='info@motorameda.com'required/>"
		."<label>Telèfon: </label><input type='text' name='telefon' placeholder='123456789' pattern='[0-9]{9}' required/>"
	."</article>"
		."<article class='boto'>"
		  ."<input type='submit' value='ENVIAR' name='envia'>"
    ."</form>"
    ."</article>";
	// Cargamos la plantilla de la web
	require_once('../Plantilla.php');
?>