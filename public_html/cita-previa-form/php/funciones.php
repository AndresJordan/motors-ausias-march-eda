<?php
// Fichero con los datos para la conexión a la base de datos
require_once "../../db/connect-db.php";
session_start();
// Si entra en session dades, es que s'està modifican, construeixo la cita
$cita = $_SESSION['datosCitaPreviaDate'][0] . " " . $_SESSION['datosCitaPreviaDate'][1] . ":00";
// Si la sessió dades existeix, vol dir que es modificació
if ($_SESSION['dades']) {
	// Si es modificació, aleshores vull conservar la matrícula, les altres dades seran noves i reescrites.
	$matricula = $_SESSION["dades"][1];
} else {
	// En cas contrari, vol dir que s'accedeix com a nova cita i s'agafa la sessió que va de la nova creació de cita.
	$matricula = $_SESSION["novaMat"];
}
if (isset($_POST['envia'])) {
	/**
	 * Variable que rep el nom de l'usuari des del form i la tracta
	 */
	$nom = htmlspecialchars($_POST['nom']);
	/**
	 * Variable que rep el cognom de l'usuari des del form i la tracta
	 */
	$cognom = htmlspecialchars($_POST['cognom']);
	/**
	 * Variable que rep el correu electrònic de l'usuari des del form i la tracta
	 */
	$email = htmlspecialchars($_POST['email']);
	/**
	 * Variable que rep el telèfon electrònic de l'usuari des del form i la tracta
	 */
	$telefon = htmlspecialchars($_POST['telefon']);
	if (preg_match("/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð]{2,48}$/", $nom) && preg_match("/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð]*$/", $cognom) && filter_var($email, FILTER_VALIDATE_EMAIL) && preg_match("([0-9]{9})", $telefon)) {
		insertarCita($nom, $cognom, $email, $telefon);
	} else {
	}
}
/**
 * Funció que s'entra un cop rep les dades del form i s'ha validat.
 * Aquest realitza la sessió depenent si s'arriba amb sessió de modificació o no, ja que voldrà dir si es per modificar o es nova.
 * Un cop fet això es crida al site de confirmacio que gestiona els inserts i updates.
 * @param $nom - Nom usuari
 * @param $cognom - Cognom usuari
 * @param $email - Correu usuari
 * @param $telefon - Telefon usuari
 */
function insertarCita($nom, $cognom, $email, $telefon) {
	global $cita, $matricula;
	// Si existeix la sessió dades, vol dir que es vol modificar, i aleshores afegim al array la variable de ID
	if ($_SESSION['dades']) {
		$resultatConsulta = array(
			$_SESSION['dades'][1],
			$nom,
			$cognom,
			$telefon,
			$email,
			$cita,
			$_SESSION['dades'][0]
		);
		// Si no existeix dades, aleshores simplement afafem totes les dades del form
	} else {
		$resultatConsulta = array(
			$matricula,
			$nom,
			$cognom,
			$telefon,
			$email,
			$cita
		);
	}
	// Creem una session amb les dades de la consulta
	$_SESSION['nouInsert'] = $resultatConsulta;
	// Envia la session a la pagina de cita previa confirmacio
	header('location:../cita-previa-confirmacio/bodyConfirmacio.php');
}
?>