$(document).ready(function() {
    // Comprobamos si cambia el estado de algún checkbox
    $('input[type="checkbox"]').on('change', function() {
        // Volvemos a ejecutar el submit del formulario
        $('#formulari-admin-manteniment').submit();
    });
});