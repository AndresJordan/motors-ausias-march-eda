<?php
	// Fichero con los datos para la conexión como usuario administrador
	require_once('../../admin/connect-admin.php');
	// Cargamos el  php necesario para los datos
  require_once('php/Manteniment.php');
	// Cargamos los estilos necesarios para la web de bodyAdminManteniment.php
	$htmlstrhead .= '<link rel="stylesheet" href="css/styleAdminManteniment.css">'
								 .'<script src="../js/jquery.js"></script>'
							 	 .'<script src="js/jsAdminManteniment.js"></script>';
	// Cargamos el código en el body de la web bodyAdminManteniment.php
	$htmlstrbdy = "<form id='formulari-admin-manteniment' method='POST' action='bodyAdminManteniment.php'>"
									."<section class='seccio-contingut'>"
										."<section class='seccio-columnas'>"
											.'<div class="flex-item"><label>Desactivar la web</label></div>'
        							.'<div class="flex-item"><label>Backup AUTO</label></div>'
        							.'<div class="flex-item"><label>Backup ARA</label></div>'
										."</section>"
										."<section class='seccio-columnas'>"
											.'<div class="flex-item"><label class="switch"><input type="checkbox" name="desactivar-web" value="desactivar-web"' . $checkDesactivarWeb . '><span class="slider round"></span></label></div>'
											.'<div class="flex-item"><label class="switch"><input type="checkbox" name="backup-auto" value="backup-auto"' . $checkBackupAuto . '><span class="slider round"></span></label></div>'
											.'<div class="flex-item"><input type="submit" name="backup-ara" value="EXECUTA"></div>'
										."</section>"
								."</section>"
							."</form>";
		// Cargamos la plantilla de la web
		require_once('../PlantillaAdmin.php');
?>