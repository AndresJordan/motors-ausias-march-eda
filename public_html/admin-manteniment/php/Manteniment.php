<?php
// Fichero con los datos para la conexión a la base de datos
require_once '../../db/connect-db.php';

// 							rename('maintenance.html.bak', 'maintenance.html');
// 							rename('maintenance.html', 'maintenance.html.bak');

pintaChckbx();
// Controlamos el click si activa/desactiva el checkbox desactivar la web
if (isset($_POST['desactivar-web'])) {
	// Obtenemos el número de chckbx activados
	$resultadoQuery = queryObtenerCheckbxAdmin();
	// Obtenemos el primer registro obtenido de la base de datos
	$row = mysqli_fetch_assoc($resultadoQuery);
	// Comprobamos si es la hora obtenida en la base de datos
	$aa = $row['desactivar_web'];
	if ($aa == 0) {
		queryCheckbxUpdateAdmin("desactivar_web", 1);
	} else {
		queryCheckbxUpdateAdmin("desactivar_web", 0);
	}
	pintaChckbx();
} else if (isset($_POST['backup-auto'])) {
	// Obtenemos el número de chckbx activados
	$resultadoQuery = queryObtenerCheckbxAdmin();
	// Obtenemos el primer registro obtenido de la base de datos
	$row = mysqli_fetch_assoc($resultadoQuery);
	// Comprobamos si es la hora obtenida en la base de datos
	$bb = $row['backup_auto'];
	if ($bb == 0) {
		queryCheckbxUpdateAdmin("backup_auto", 1);
	} else {
		queryCheckbxUpdateAdmin("backup_auto", 0);
	}
	pintaChckbx();
} else if (empty($_POST['desactivar-web'])) {
	// Obtenemos el número de chckbx activados
	$resultadoQuery = queryObtenerCheckbxAdmin();
	// Obtenemos el primer registro obtenido de la base de datos
	$row = mysqli_fetch_assoc($resultadoQuery);
	// Comprobamos si es la hora obtenida en la base de datos
	$aa = $row['desactivar_web'];
	if ($aa == 0) {
		queryCheckbxUpdateAdmin("desactivar_web", 1);
	} else {
		queryCheckbxUpdateAdmin("desactivar_web", 0);
	}
	pintaChckbx();
} else if (empty($_POST['backup-auto'])) {
	// Obtenemos el número de chckbx activados
	$resultadoQuery = queryObtenerCheckbxAdmin();
	// Obtenemos el primer registro obtenido de la base de datos
	$row = mysqli_fetch_assoc($resultadoQuery);
	// Comprobamos si es la hora obtenida en la base de datos
	$bb = $row['backup_auto'];
	if ($bb == 0) {
		queryCheckbxUpdateAdmin("backup_auto", 1);
	} else {
		queryCheckbxUpdateAdmin("backup_auto", 0);
	}
	pintaChckbx();
}
// // Controlamos el click si activa/desactiva el checkbox backup auto
//  if(isset($_POST['backup-auto'])) {
// 	 // Obtenemos el número de chckbx activados
// 	$resultadoQuery = queryObtenerCheckbxAdmin();
//  	// Obtenemos el primer registro obtenido de la base de datos
//    $row = mysqli_fetch_assoc($resultadoQuery);
// 	 // Comprobamos si es la hora obtenida en la base de datos
// 		$bb = $row['backup_auto'];
// 	 if ($bb == 0) {
// 			queryCheckbxUpdateAdmin("backup_auto", 1);
// 		} else {
// 			queryCheckbxUpdateAdmin("backup_auto", 0);
// 		}
// 	 pintaChckbx();
//  }
function pintaChckbx() {
	global $checkDesactivarWeb, $checkBackupAuto;
	// Obtenemos el número de chckbx activados
	$resultadoQuery = queryObtenerCheckbxAdmin();
	// Obtenemos el primer registro obtenido de la base de datos
	$row = mysqli_fetch_assoc($resultadoQuery);
	// Comprobamos si es la hora obtenida en la base de datos
	$aa = $row['desactivar_web'];
	// 	$bb = $row['backup_auto'];
	if ($aa == 0) {
		$checkDesactivarWeb = "";
	} else {
		$checkDesactivarWeb = "checked";
	}
	// 	 if ($bb == 0) {
	// 		$checkBackupAuto = "";
	// 	 } else {
	// 		$checkBackupAuto = "checked";
	// 	 }
}
if (isset($_POST['backup-ara'])) {
}
// Función que usaremos para obtener los checkbx activados y no
function queryObtenerCheckbxAdmin() {
	global $db_server, $db_database, $db_table_admin;
	// Abrimos la conexión con la base de datos
	obrirConexioDB();
	// Nos conectamos a la base de datos
	if (mysqli_select_db($db_server, $db_database)) {
		// Creamos la query para obtener los checkbx activados y no
		$queryChckbxAdmin = "SELECT * FROM  `$db_table_admin` ";
		// Realizar consulta a la base de datos
		$resultadoQuery = mysqli_query($db_server, $queryChckbxAdmin);
		// Nos desconectamos de la base de datos
		tancarConexioDB();
		// Retornamos el número de filas totales como resultado
		return $resultadoQuery;
	}	else {
		die("No s'ha pogut establir la conexió. " . mysqli_error($db_server));
	}
}
// Función que usaremos para obtener los checkbx activados y no
function queryCheckbxUpdateAdmin($campo, $valor) {
	global $db_server, $db_database, $db_table_admin;
	// Abrimos la conexión con la base de datos
	obrirConexioDB();
	// Nos conectamos a la base de datos
	if (mysqli_select_db($db_server, $db_database)) {
		// Creamos la query para obtener los checkbx activados y no
		$queryChckbxAdmin = "UPDATE `$db_database`.`$db_table_admin` SET `$campo`=$valor";
		// Realizar consulta a la base de datos
		$resultadoQuery = mysqli_query($db_server, $queryChckbxAdmin);
		// Nos desconectamos de la base de datos
		tancarConexioDB();
	}	else {
		die("No s'ha pogut establir la conexió. " . mysqli_error($db_server));
	}
}
?>