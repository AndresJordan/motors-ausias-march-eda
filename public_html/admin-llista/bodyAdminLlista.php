<?php
	// Fichero con los datos para la conexión como usuario administrador
	require_once('../../admin/connect-admin.php');
  // Cargamos el  php necesario para los datos de dia
	require_once('php/Hores.php');
	// Cargamos los estilos necesarios para la web de Cita-Previa-Date.php
	$htmlstrhead .= '<link rel="stylesheet" href="css/styleAdminLlista.css">'
							 	 .'<script src="../js/jquery.js"></script>'
							 	 .'<script src="../js/js-cookie.js"></script>'
							 	 .'<script src="js/jsCitaPreviaDate.js"></script>';

     // Cargamos el código en el body de la web Cita-Previa-Date.php
   	$htmlstrbdy .= "<article class='form'><form id='formulari-cita-previa-date' method='POST' action='bodyAdminLlista.php'>"
   										."<section class='escull-data'>"
   											."<p>Data: <span>"
   											.'<input type="date" class="data-escollida" name="data-escollida" id="data-escollida" step="1" min="' . $fechaActual . '" value="' . $fechaEscogida . '">'
   											."</span></p>"
   										."</section>"
   								."</form></article>";
	// Cargamos php que realiza la query del dia seleccionado
	require_once('php/DiaSelect.php');
  // Cargamos la plantilla de la web
	require_once('../PlantillaAdmin.php');
?>
