$(document).ready(function() {
    // Comprobamos si cambia de fecha en el input date 
    $("#data-escollida").change(function() {
        // Obtenemos la fecha introducida
        var fechaEscogida = Date.parse($(this).val());
        // Obtenemos la fecha actual  
        var fechaActual = new Date();
        // Calculamos la fecha minima
        var fechaMinima = Date.parse(fechaActual.getFullYear() + "/" + (fechaActual.getMonth() + 1) + "/" + fechaActual.getDate());
        // Incrementamos 6 meses a la fecha minima
        fechaActual.setMonth(fechaActual.getMonth() + 7);
        // Calculamos la fecha maxima
        var fechaMaxima = Date.parse(fechaActual.getFullYear() + "/" + fechaActual.getMonth() + "/" + fechaActual.getDate());
        // Obtenemos la fecha anterior escogida
        var fechaAnteriorEscogida = Cookies.get('fechaAnteriorEscogida');
        // Si la fecha escogida está entre la minima y la maxima se le acepta el formulario
        if (fechaEscogida <= fechaMaxima && fechaEscogida >= fechaMinima && fechaAnteriorEscogida != fechaEscogida) {
            // En caso que cambie de fecha hacemos la peticion al servidor a través del formulario
            $('#formulari-cita-previa-date').submit();
            // Asignamos la fecha anterior
            Cookies.set('fechaAnteriorEscogida', fechaEscogida);
        }
    });
    // Deshabilitamos la tecla intro del formulario
    $('input').on('keydown', function(event) {
        if (event.which == 13) {
            event.preventDefault();
        }
    });
});