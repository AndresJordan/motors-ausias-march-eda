<?php
// Asignamos nuestra zona horaria
date_default_timezone_set('Europe/Madrid');
// Obtenemos la fecha actual
$fechaActual =  date("Y-m-d");
// Obtenemos la fecha escogida
$fechaEscogida;
// Comprobamos si ha cambiado de fecha
if (!empty($_POST)) {
	// Obtenemos la fecha escogida
	$fechaEscogida =  $_POST['data-escollida'];
} else {
	// Sino la fecha escogida es la actual
	$fechaEscogida = $fechaActual;
}
?>
