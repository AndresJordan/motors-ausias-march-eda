<?php
// Iniciamos la sesión
session_start();
// Fichero con los datos para la conexión a la base de datos
require_once '../../db/connect-db.php';
// Variables globals importades de connect-db
global $db_server, $db_database, $db_table,$campoDataCita;
obrirConexioDB();
if (mysqli_select_db($db_server, $db_database)) {
	$query = "SELECT * FROM $db_database.$db_table WHERE DATE_FORMAT($campoDataCita,'%Y-%m-%d') = '$fechaEscogida'";
  //Realitzar consulta a la base de dades
  $result = mysqli_query($db_server, $query);
    if($result) {
			$rows = mysqli_num_rows($result);
			if ($rows == 0){
				$htmlstrbdy .= "<article class='container'>"
				."<h1>No hi ha cites al dia marcat</h1>";
			} else {
	      $htmlstrbdy .= "<article class='container'>"
	      ."<h1>Llista de pròximes cites</h1>";
		      while($row = $result->fetch_assoc()) {
		      		$htmlstrbdy .= "<section class='card'>";
		      		$htmlstrbdy .= "<section class='column'>";
		      			$htmlstrbdy .= '<p>ID</p>';
		      			$htmlstrbdy .= '<p>MATRICULA</p>';
		      			$htmlstrbdy .= '<p>NOM</p>';
		      			$htmlstrbdy .= '<p>COGNOM</p>';
		      			$htmlstrbdy .= '<p>TELEFON</p>';
		      			$htmlstrbdy .= '<p>CORREU</p>';
		      			$htmlstrbdy .= '<p>CITA</p>';
		      		$htmlstrbdy .= "</section>";
		      		$htmlstrbdy .= "<section class='info'>";
		      			$htmlstrbdy .= '<p>'.$row["id"]."</p>";
		      			$htmlstrbdy .= '<p>'.$row["matricula"]."</p>";
		      			$htmlstrbdy .= '<p>'.$row["nom"]."</p>";
		      			$htmlstrbdy .= '<p>'.$row["cognom"]."</p>";
		      			$htmlstrbdy .= '<p>'.$row["telefon"]."</p>";
		      			$htmlstrbdy .= '<p>'.$row["correu"]."</p>";
		            $formatDateES = date("d-m-Y H:i", strtotime($row["data_cita"]));
		      			$htmlstrbdy .= '<p>'.$formatDateES."h</p>";
		      		$htmlstrbdy .= "</section>";
		      		$htmlstrbdy .= "</section>";
		      }
			}
      $htmlstrbdy .="</article>";
    } else {
        $htmlstrbdy .="<h1>Error al mostrar la llista</h1>";
      }
} else {
        $htmlstrbdy .="<h1>Error al conectar a la base de dades</h1>";
}
  mysqli_free_result($result);
  tancarConexioDB();
?>
