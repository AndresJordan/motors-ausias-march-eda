<?php
session_start();
$dades = $_SESSION["dades"];
$id = $dades[0];
// Fichero con los datos para la conexión a la base de datos
require_once "../../db/connect-db.php";

// Si l'usuari clica a MODIFICAR, anirem a realitzar el procés de modificar, si clica a ELIMINAR, eliminarem el registre, i en cas de vindre de nou, mostrarem la modificació abans de res.
if (isset($_POST['MODIFICAR'])) {
	modificar();
} else if (isset($_POST['ELIMINAR'])) {
	eliminar();
} else {
	mostrarModificacion();
}
/**
 * Funció que mostra al usuari que la matrícula ja existeix, i li pregunta que vol fer, amb els submits ELIMINAR i MODIFICAR.
 * ELIMINAR crida a la funció eliminar();
 * MODIFICAR crida a la funció modificar();
 */
function mostrarModificacion() {
	global $htmlstrbdy,$dades;
    $htmlstrbdy .= "<h1> Modificació de la cita </h1>"
			."<article class='confirmacio'>"
				."<section class='dades'>"
					."<p class='titol'>Matrícula</p>"
					."<p class='dada'>". htmlspecialchars(strtoupper($dades[1]))."<p>"
					."<p class='titol'>Nom</p>"
					."<p class='dada'>".htmlspecialchars($dades[2])."<p>"
					."<p class='titol'>Cognom</p>"
					."<p class='dada'>".htmlspecialchars($dades[3])."<p>"
					."<p class='titol'>Telefon</p>"
					."<p class='dada'>".htmlspecialchars($dades[4])."<p>"
					."<p class='titol'>Correu</p>"
					."<p class='dada'>".htmlspecialchars($dades[5])."<p>"
					."<p class='titol'>Cita</p>"
					."<p class='dada'>".htmlspecialchars($dades[6])."<p>"
				."</section>"
				."<section class='buto'>"
					."<form action='bodyModificacio.php' onsubmit='return confirmacio()' method='post'>"
						."<input type='hidden' name='matricula' value='".htmlspecialchars($dades[1])."'>"
						."<input type='submit' class='buto-modificar' name='MODIFICAR' value='MODIFICAR'> "
						."<input type='submit' class='buto-eliminar' name='ELIMINAR' value='ELIMINAR'>"
					."</form>"
				."</section>"
			."</article>";
	}
/**
 * Funció que elimina el registre un cop l'usuari ho ha confirmat.
 * Fa la query a la base de dades i l'elimina per l'ID (PK).
 * Un cop fet es mostra a l'usuari la finalització del procés i elimina la sessió.
 */
function eliminar() {
	obrirConexioDB();
	global $db_server, $db_database, $db_table, $idCita, $htmlstrbdy, $id;
	if (mysqli_select_db($db_server, $db_database)) {
		$query = "DELETE FROM $db_database.$db_table WHERE $idCita=$id";
		// Realitzar consulta a la base de dades
		$result = mysqli_query($db_server, $query);
		if (mysqli_affected_rows($db_server) > 0) {
			$htmlstrbdy = "<article class='container'>" 
			. "<h1>Cita esborrada correctament!</h1>" 
			. "</article>";
		} else {
			$htmlstrbdy = "<article class='container'>" 
							. "<h1>Error al esborrar la cita</h1>" 
						. "</article>";
		}
	} else {
		$htmlstrbdy = "<article class='container'>" 
						. "<h1>Error al conectar amb la base de dades</h1>" 
					. "</article>";
	}
	
	session_destroy();
	mysqli_free_result($result);
	tancarConexioDB();
}
/**
 * Si vol modificar, simplement anem al site de tornar a escollir data.
 * Com la sessió té les dades ja, i es una única sessió per noves cites i modificades, no l'eliminem.
 */
function modificar() {
	header('location:../cita-previa-date/bodyCitaPreviaDate.php');
}
?>
