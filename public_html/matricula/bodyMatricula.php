<?php
	// Cargamos el php necesario para los datos
	require_once 'php/funciones.php';
	// Cargamos los estilos necesarios para la web de bodyMatricula.php
	$htmlstrhead = '<link rel="stylesheet" href="css/styleMatricula.css">';
	// Cargamos el código en el body de la web de bodyMatricula.php
	$htmlstrbdy ="<section class='contingut'>"
			."<header class='matricula'>"
				."Introdueix la matrícula"
			."</header>"
			."<article class='entraMatricula'>"
				."<form action='bodyMatricula.php' method='post' name='formMatricula' id='formMatricula'>"
					."<input type='text' name='matricula' placeholder='0000BBB' pattern = '^\d{4}[BCDFGHJKLMNPRSTVWXYZbcdfghjklmnprstvwxyz]{3}$' required>"
					."<br><br>"
					."<input class='boto' type='submit' value='PROCEDEIX' name='procedeix' />"
				."</form>"
			."</article>"		
		."</section>";
	// Cargamos la plantilla de la web
	require_once('../Plantilla.php');
?>