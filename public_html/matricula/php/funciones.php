<?php
// Fichero con los datos para la conexión a la base de datos
require_once "../../db/connect-db.php";
session_start();
// Si es rep el POST de procedeix, l'usuari ha introduit la matrícula i la validarem doblemente, per html5 i per php.
if (isset($_POST['procedeix'])) {
	$matricula = $_POST['matricula'];
	if (preg_match("(^\d{4}[BCDFGHJKLMNPRSTVWXYZ]{3}$)", strtoupper($matricula))) {
		queryMatricula($matricula);
	} else {
	}
}
/**
 * Funció que realitza una query a la BD per comprovar si la matrícula ja existeix i canviar el tractament del procés
 * Si no dóna resultat, crea la sessió novaMat i crida a escollir data.
 * Si existeix aquesta, agafa les dades de la BD, crea un array amb el contingut, l'afegeix a la sessió 'dades' i l'envia al site de modificació per si cal modificar o esborrar aquesta cita.
 */
function queryMatricula($matricula) {
	global $db_server, $db_database, $db_table, $campoMatricula;
	obrirConexioDB();
	if (mysqli_select_db($db_server, $db_database)) {
		$queryMatricula = "SELECT * FROM `$db_database`.`$db_table` WHERE $campoMatricula = '$matricula'";
		// Realitzar consulta a la base de dades
		$result = mysqli_query($db_server, $queryMatricula);
		// Comprovar si la consulta es vuida
		$rows = mysqli_num_rows($result);
		if ($rows == 0) {
			$_SESSION['novaMat'] = strtoupper($matricula);
			header('Location: ../cita-previa-date/bodyCitaPreviaDate.php');
			die();
		} else {
			// Agafem el resultat de la consulta i el guardem en un array
			$row = mysqli_fetch_assoc($result);
			$resultatConsulta = array(
				$row["id"],
				strtoupper($row["matricula"]) ,
				$row["nom"],
				$row["cognom"],
				$row["telefon"],
				$row["correu"],
				$row["data_cita"],
				$row["data_reserva"]
			);
			// Creem una session amb les dades de la consulta
			$_SESSION['dades'] = $resultatConsulta;
			// Envia la session a la pagina de cita previa
			header('location:../cita-previa-modificacio/bodyModificacio.php');
			die();
			}
		} else {
		die('Invalid connect : ' . mysqli_error($db_server));
		}
	mysqli_free_result($result);
	tancarConexioDB();
	}
?>