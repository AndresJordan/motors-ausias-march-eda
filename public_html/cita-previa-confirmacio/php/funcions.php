<?php
session_start();
// Fichero con los datos para la conexión a la base de datos
require_once "../../db/connect-db.php";
// Si la sessió nouInsert no existeix, aleshores no interessa entrar aquí, el fem fora a Inici
if (isset($_SESSION["nouInsert"])) {
	$nouInsert = $_SESSION["nouInsert"];
	// Si existeix i el contingut es de 6, aleshores vol dir que es nova la cita, en cas contrari es modificar
	if (count($nouInsert) == 6) {
		confirmarCita();
	} else {
		confirmarModificar();
	}
} else {
	header('location:../inici/Inici.php');
}
// Si un cop mostrada les dades, clica a confirmar, farem el insert nou, en cas d'haver mostrat modificar, anirà a insertar modificació
if (isset($_POST['CONFIRMAR'])) {
	insertarNou();
} else if (isset($_POST['MODIFICAR'])) {
	insertarModificacio();
} else if (isset($_POST['INICI'])) {
	session_destroy();
	header('location:../inici/Inici.php');
}
/**
 *		Funció per a mostrar totes les dades modificades de la cita abans d'actualizarles.
 *		S'accedeix a través de la sessió, si no hi ha creada cap sessió torna a inici.
 *		Si l'usuari accepta les dades (submit MODIFICAR) es crida a la funció insertarModificacio()
 */
function confirmarModificar(){
  global $htmlstrbdy,$nouInsert;
    $htmlstrbdy .= "<h1> Modificació de la cita </h1>"
      ."<article class='confirmacio'>"
      ."<section class='dades'>"
      ."<p>Matrícula</p>"
      ."<p class='dada'>".htmlspecialchars(strtoupper($nouInsert[0]))."<p>"
      ."<p>Nom</p>"
      ."<p class='dada'>".htmlspecialchars($nouInsert[1])."<p>"
      ."<p>Cognom</p>"
      ."<p class='dada'>".htmlspecialchars($nouInsert[2])."<p>"
      ."<p>Telèfon</p>"
      ."<p class='dada'>".htmlspecialchars($nouInsert[3])."<p>"
      ."<p>Correu</p>"
      ."<p class='dada'>".htmlspecialchars($nouInsert[4])."<p>"
      ."<p>Cita</p>"
      ."<p class='dada'>".htmlspecialchars($nouInsert[5])."<p>"
      ."<p>ID</p>"
      ."<p class='dada'>".htmlspecialchars($nouInsert[6])."<p>"
      ."</section>"
      ."<section class='buto'>"
      ."<form id='submit' action='bodyConfirmacio.php' onsubmit='return confirmacio()' method='post'>"
      ."<input type='submit' class='buto-afegir' name='MODIFICAR' value='MODIFICAR'> "
      ."<input type='submit' class='buto-afegir' name='INICI' value='INICI'>"
      ."</form>"
      ."</section>"
      ."</article>";
}

/**
 *		Funció per a mostrar totes les dades noves de la cita abans d'insertar-les.
 *		S'accedeix a través de la sessió, si no hi ha creada cap sessió torna a inici.
 *		Si l'usuari accepta les dades (submit CONFIRMAR) es crida a la funció insertarNou()
 */
function confirmarCita(){
    global $htmlstrbdy,$nouInsert;
      $htmlstrbdy .= "<h1> Confirmació de la cita </h1>"
        ."<article class='confirmacio'>"
        ."<section class='dades'>"
        ."<p class='titol'>Matrícula</p>"
        ."<p class='dada'>".htmlspecialchars(strtoupper($nouInsert[0]))."<p>"
        ."<p class='titol'>Nom</p>"
        ."<p class='dada'>".htmlspecialchars($nouInsert[1])."<p>"
        ."<p class='titol'>Cognom</p>"
        ."<p class='dada'>".htmlspecialchars($nouInsert[2])."<p>"
        ."<p class='titol'>Telèfon</p>"
        ."<p class='dada'>".htmlspecialchars($nouInsert[3])."<p>"
        ."<p class='titol'>Correu</p>"
        ."<p class='dada'>".htmlspecialchars($nouInsert[4])."<p>"
        ."<p class='titol'>Cita</p>"
        ."<p class='dada'>".htmlspecialchars($nouInsert[5])."<p>"
        ."</section>"
        ."<section class='buto'>"
        ."<form id='submit' action='bodyConfirmacio.php' onsubmit='return confirmacio()' method='post'>"
        ."<input type='submit' class='buto-afegir' name='CONFIRMAR' value='CONFIRMAR'> "
        ."<input type='submit' class='buto-afegir' name='INICI' value='INICI'>"
        ."</form>"
        ."</section>"
        ."</article>";
  }
  
/**
 *		Funció per a realitzar l'actualització de les dades del client. UPDATE
 *		Si es correcte o no es mostra al usuari i s'elimina la sessió
 */
function insertarModificacio() {
	global $db_server, $db_database, $db_table, $idCita, $campoMatricula, $camposRequeridos, $campoDataCita, $htmlstrbdy, $nouInsert;
	obrirConexioDB();
	if (mysqli_select_db($db_server, $db_database)) {
		$query = "UPDATE $db_database.$db_table SET $camposRequeridos[0]=\"$nouInsert[1]\",$camposRequeridos[1]=\"$nouInsert[2]\",$camposRequeridos[2]=\"$nouInsert[3]\",$camposRequeridos[3]=\"$nouInsert[4]\",$campoDataCita=\"$nouInsert[5]\" WHERE $idCita=$nouInsert[6]";
		// Realitzar consulta a la base de dades
		$result = mysqli_query($db_server, $query);
		if (mysqli_affected_rows($db_server) > 0) {
			$htmlstrbdy = "<article class='container'>"
			  ."<h1>Cita modificada correctament</h1>"
			  ."</article>"
			  ."<article class='confirmacio'>"
			  ."<section class='dades'>"
			  ."<p>Matrícula</p>"
			  ."<p class='dada'>".htmlspecialchars($nouInsert[0])."<p>"
			  ."<p>Nom</p>"
			  ."<p class='dada'>".htmlspecialchars($nouInsert[1])."<p>"
			  ."<p>Cognom</p>"
			  ."<p class='dada'>".htmlspecialchars($nouInsert[2])."<p>"
			  ."<p>Telèfon</p>"
			  ."<p class='dada'>".htmlspecialchars($nouInsert[3])."<p>"
			  ."<p>Correu</p>"
			  ."<p class='dada'>".htmlspecialchars($nouInsert[4])."<p>"
			  ."<p>Cita</p>"
			  ."<p class='dada'>".htmlspecialchars($nouInsert[5])."<p>"
			  ."<p>ID</p>"
			  ."<p class='dada'>".htmlspecialchars($nouInsert[6])."<p>"
			  ."</section>"
			  ."</article>";
		  
          session_destroy();		
		  } else {
			$htmlstrbdy = "<article class='container'>" 
							. "<h1>Error al modificar la cita!</h1>" 
						. "</article>";
		}
	} else {
		$htmlstrbdy = "<article class='container'>" . "<h1>Error de connexió amb la base de dades</h1>" . "</article>";
	}
	session_destroy();
	mysqli_free_result($result);
	tancarConexioDB();
}
/**
 *		Funció per fer l'insert d'un nou registre acceptat per l'usuari.INSERT
 *		Si es correcte o no es mostra al usuari i s'elimina la sessió
 */
function insertarNou() {
	global $db_server, $db_database, $db_table, $idCita, $campoMatricula, $camposRequeridos, $campoDataCita, $htmlstrbdy, $nouInsert;
	obrirConexioDB();
	if (mysqli_select_db($db_server, $db_database)) {
		$query = "INSERT INTO $db_database.$db_table($campoMatricula,$camposRequeridos[0],$camposRequeridos[1],$camposRequeridos[2],$camposRequeridos[3],$campoDataCita) values (\"$nouInsert[0]\",\"$nouInsert[1]\",\"$nouInsert[2]\",\"$nouInsert[3]\",\"$nouInsert[4]\",\"$nouInsert[5]\")";
		// Realitzar consulta a la base de dades
		$result = mysqli_query($db_server, $query);
		if (mysqli_affected_rows($db_server) > 0) {
				$htmlstrbdy = "<article class='container'>"
			  ."<h1>Cita registrada correctament</h1>"
			  ."</article>"
			  ."<article class='confirmacio'>"
			  ."<section class='dades'>"
			  ."<p>Matrícula</p>"
			  ."<p class='dada'>".htmlspecialchars(strtoupper($nouInsert[0]))."<p>"
			  ."<p>Nom</p>"
			  ."<p class='dada'>".htmlspecialchars($nouInsert[1])."<p>"
			  ."<p>Cognom</p>"
			  ."<p class='dada'>".htmlspecialchars($nouInsert[2])."<p>"
			  ."<p>Telèfon</p>"
			  ."<p class='dada'>".htmlspecialchars($nouInsert[3])."<p>"
			  ."<p>Correu</p>"
			  ."<p class='dada'>".htmlspecialchars($nouInsert[4])."<p>"
			  ."<p>Cita</p>"
			  ."<p class='dada'>".htmlspecialchars($nouInsert[5])."<p>"
			  ."</section>"
			  ."</article>";
		  } else {
			$htmlstrbdy = "<article class='container'>" 
							. "<h1>Error al registrar la cita!</h1>" 
						. "</article>";
		}
	} else {
		$htmlstrbdy = "<article class='container'>" 
						. "<h1>Error de connexió amb la base de dades</h1>" 
					. "</article>";
	}
	session_destroy();
	mysqli_free_result($result);
	tancarConexioDB();
}
?>
