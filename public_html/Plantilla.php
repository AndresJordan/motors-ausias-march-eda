<!DOCTYPE html>

<head>
    <meta charset="utf8">
    <link rel="stylesheet" href="../css/Plantilla.css">
    <?php echo $htmlstrhead; ?>
</head>

<body>
    <header>
        <figure>
            <img class="logo" src="../img/logo.png">
            <img class='signature' src="../img/brand.png">
        </figure>

        <nav>
            <ul class="nav">
                <li class="nav-item"> <a href="../inici/Inici.php">Inici</a> </li>
                <li class="nav-item"> <a href="../matricula/bodyMatricula.php">Cita prèvia</a> </li>
                <li class="nav-item"> <a href="../que-es-itv/bodyItv.php">Què és l'ITV</a> </li>
            </ul>
        </nav>
    </header>
    <main class="content">
        <?php echo $htmlstrbdy; ?>
    </main>
    <footer class="footer">
        <article class="avisos">
            <section class="sidebyside">
                <h3>Ausiàs March Motors</h3>
                <p>Taller líder d'ITV a la ciutat comtal des de 2010</p>
                <p>La vostra confiança és el nostre negoci</p>
            </section>
            <section class="sidebyside">
                <h3>Avís legal</h3>
                <p><a href="">Termes d'ús</a></p>
                <p><a href="">Política de privacitat</a></p>
                <p><a href="">Avis legal i cookies</a></p>
                <p><a href="">Condicions de contractació</a></p>
            </section>
            <section class="sidebyside">
                <h3>Contacte</h3>
                <p>
                    Avinguda Esplugues, 38
                </p>
                <p>
                    08034 Barcelona
                </p>
                <p>
                    932 03 33 32
                </p>
                <p>
                    <a href="">info@motorameda.cat</a>
                </p>
            </section>
        </article>
        <section class="copyright">
            <span class="copyright">© Copyright 2017</span>
        </section>
    </footer>
</body>

</html>