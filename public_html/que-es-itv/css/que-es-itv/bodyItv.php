<?php

	$htmlstrhead .= '<link rel="stylesheet" href="css/styleItv.css">';

	$htmlstrbdy .= "<article class='itv'>"
			."<h1> Inspecció tecnica de vehicles </h1>"
			."Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam aliquam, dolor sed eleifend accumsan, "
			."augue mauris dignissim dui, quis aliquam lacus eros vel magna. Aenean tristique velit non nibh luctus, dignissim "
			."laoreet velit sagittis. Etiam viverra mauris quis vulputate accumsan. Mauris lectus sem, eleifend ut luctus at, tempor "
			."et odio. Cras blandit magna a nisi interdum volutpat. In hac habitasse platea dictumst. Curabitur tortor orci, fermentum "
			."ac risus ut, condimentum tempus augue. Suspendisse at massa ac neque sodales mattis at ac magna. Mauris orci urna, mattis "
			."ut pulvinar nec, ornare at elit. Aenean tempus nunc pellentesque enim varius, ut malesuada eros feugiat. Nullam feugiat nunc "
			."vel interdum venenatis. Fusce et porta est, nec varius felis. Nunc interdum egestas enim, vel pretium eros facilisis ut. "
		."</article>"

		."<section class='proces'>"
			."<h1>Proces</h1>"

			."<section class='arriba'>"
				."<section class='section1'>"
					."<figure>"
						."<img class ='logo' src='../img/logo.png'>"
					."</figure>"
					."<article>"
						."Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam aliquam, dolor sed eleifend accumsan"
					."</article>"
				."</section>"

				."<section class='section2'>"
					."<figure>"
						."<img class ='logo' src='../img/logo.png'>"
					."</figure>"
					."<article>"
						."Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam aliquam, dolor sed eleifend accumsan"
					."</article>"
				."</section>"

				."<section class='section3'>"
					."<figure>"
						."<img class ='logo' src='../img/logo.png'>"
					."</figure>"
					."<article>"
						."Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam aliquam, dolor sed eleifend accumsan"
					."</article>"
				."</section>"

			."</section>"

			."<section class='abajo'>"

				."<section class='section4'>"
					."<figure>"
						."<img class ='logo' src='../img/logo.png'>"
					."</figure>"
					."<article>"
						."Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam aliquam, dolor sed eleifend accumsan"
					."</article>"
				."</section>"

				."<section class='section5'>"
					."<figure>"
						."<img class ='logo' src='../img/logo.png'>"
					."</figure>"
					."<article>"
						."Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam aliquam, dolor sed eleifend accumsan"
					."</article>"
				."</section>"

				."<section class='section6'>"
					."<figure>"
						."<img class ='logo' src='../img/logo.png'>"
					."</figure>"
					."<article>"
						."Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam aliquam, dolor sed eleifend accumsan"
					."</article>"
				."</section>"

		."</section>"
	."</section>";

	require_once('../Plantilla.php');
?>
