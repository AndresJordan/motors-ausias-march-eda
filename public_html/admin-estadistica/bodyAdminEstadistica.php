<?php
	// Fichero con los datos para la conexión como usuario administrador
	require_once('../../admin/connect-admin.php');
 	// Fichero con los datos para la conexión a la base de datos
	require_once('php/funcions.php');
	// Cargamos los estilos necesarios para la web de bodyAdminEstadistica.php
	$htmlstrhead .= '<link rel="stylesheet" href="css/styleAdminEstadistica.css">';
	// Cargamos la plantilla de la web
	require_once('../PlantillaAdmin.php');
?>