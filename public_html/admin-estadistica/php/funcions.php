<?php
// Fichero con los datos para la conexión a la base de datos
require_once "../../db/connect-db.php";
/*
*	Variables globals importades de connect-db
*/
global $db_server, $db_database, $db_table, $campoDataCita;
/*
*	Obrim la connexió a la base de dades
*/
obrirConexioDB();
/*
*	Si la connexió a la base de dades es correcta, entrem
*/
if (mysqli_select_db($db_server, $db_database)) {
	/*
	*	Fem la query per les estadístiques diaries
	*/
	$query = "select date($campoDataCita) as DIA, TRUNCATE(count(*)/48*100,2) as 'Numero cites', count(*) as 'CITES' from $db_table group by DIA";
	// Realitzar consulta a la base de dades
	$result = mysqli_query($db_server, $query);
	if ($result) {
		$htmlstrbdy.= "<article class='container'>" . "<h1>Percentatge ocupació del taller</h1>";
		// Fem per cada row una caixa propia visual
		while ($row = $result->fetch_assoc()) {
			$htmlstrbdy.= "<section class='card'>";
			$htmlstrbdy.= "<section class='column'>";
			$formatDateES = date("d-m-Y", strtotime($row['DIA']));
			$htmlstrbdy.= '<p>' . $formatDateES . '</p>';
			$htmlstrbdy.= "</section>";
			$htmlstrbdy.= "<section class='info'>";
			$htmlstrbdy.= '<p>' . $row['CITES'] . " Reserves | " . $row['Numero cites'] . " %</p>";
			$htmlstrbdy.= "</section>";
			$htmlstrbdy.= "</section>";
		}
		$htmlstrbdy.= "</article>";
	} else {
		$htmlstrbdy.= "<h1>Error al mostrar la llista</h1>";
	}
} else {
	$htmlstrbdy.= "<h1>Error en la conexió a la base de dades</h1>";
}
mysqli_free_result($result);
tancarConexioDB();
?>
