<?php
	// Caragamos el php necesario para el slider
	require_once('php/SliderImages.php');
	// Cargamos los estilos necesarios para la web de inici.php
	$htmlstrhead = '<link rel="stylesheet" href="css/Inici.css">'
	.'<link rel="stylesheet" href="css/IniciSliderImages.css">';
	// Cargamos el código en el body de la web inici.php
	$htmlstrbdy = "<section class='slider-images'>"
									. $htmlStrSlider
								."</section>"
							."<section class='contingut'>"
								. "<section class='qui-som'>"
									."<header>"
										."<h1>"
											."Qui som"
										."</h1>"
									."</header>"
									. "<article>"
										. "<p>Som una organització que col·labora amb usuaris, professionals del sector de l'automoció i administracions públiques
											per millorar la seguretat viària i el medi ambient a les nostres carreteres.</p>
											<p>Som l'empresa líder a Espanya d'inspecció tècnica de vehicles, on cada any confien en els nostres serveis més de
											3 milions d'usuaris.</p>
											<p>Comptem amb 1 centre d'inspecció tècnica de vehicles (ITV).</p>
											<p>La nostra xarxa d'estacions està present a Catalunya.</p>"
									."</article>"
								."</section>"
								."<section class='on-estem'>"
									."<header>"
										."<h1>"
											."On estem"
										."</h1>"
									."</header>"
									."<figure>"
										."<img class ='on-estem-img' src='img/on-estem.png'>"
									."</figure>"
								."</section>"
							."</section>";
	// Cargamos la plantilla de la web
	require_once('../Plantilla.php');

?>
