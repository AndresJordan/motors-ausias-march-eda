<?php
// Fichero con los datos para la conexión a la base de datos
require_once '../../db/connect-db.php';
// Iniciamos la sesión
session_start();
// Asignamos nuestra zona horaria
date_default_timezone_set('Europe/Madrid');
// Obtenemos la fecha actual
$fechaActual = date("Y-m-d");
// Obtenemos la hora actual
$horaActual = date("H:i");
// Obtenemos la fecha escogida
$fechaEscogida;
// Comprobamos si ha cambiado de fecha
if (!empty($_POST)) {
    // Obtenemos la fecha escogida
    $fechaEscogida = $_POST['data-escollida'];
} else {
    // Sino la fecha escogida es la actual
    $fechaEscogida = $fechaActual;
}
// Calculamos la fecha limite a partir de la fecha actual (6 meses como máximo)
$fechaLimite = date("Y-m-d", strtotime(date("Y-m-d", strtotime($fechaActual)) . " +6 month"));
// Comprobamos si ha hecho un submit
if (!isset($_POST['submit'])) {
    // Obtenemos el número de citas a partir de la hora
    $resultadoQuery = queryObtenerCitasFecha();
    // Generamos las horas de 08:00h a 20:00h
    $horaInicial = "08:00";
    $htmlstrHoras = "<table class='escull-hores-taula'>";
    $htmlstrHoras.= "<thead>";
    $htmlstrHoras.= "<tr>";
    $htmlstrHoras.= "<th colspan=2>Hora</th>";
    $htmlstrHoras.= "</tr>";
    $htmlstrHoras.= "</thead>";
    $htmlstrHoras.= "<tbody>";
    // Creamos un contador con el número de horas que se puede solicitar citas
    $i = 0;
    // Obtenemos el primer registro obtenido de la base de datos
    $row = mysqli_fetch_assoc($resultadoQuery);
    // Recorremos cada tramo de 30 minutos
    while ($i < 24) {
        // Si es par creamos una fila nueva
        if ($i % 2 == 0) {
            $htmlstrHoras.= "<tr>";
        }
        // Comprobamos si la fecha escogida es sábado o domingo, ese día no abre el taller
        if (date("N", strtotime($fechaEscogida)) == 6 || date("N", strtotime($fechaEscogida)) == 7) {
          $htmlstrHoras.= "<td class='escull-hores-taula-ocupat'>";
        // Comprobamos si es la hora obtenida en la base de datos
        } else if ($row['keyHora'] == $horaInicial) {
            // Si es la hora comprobamos el número de citas y la mostramos como disponible o no
            if ($row['numeroCitas'] < 2) {
                $htmlstrHoras.= "<td>";
            } else {
                $htmlstrHoras.= "<td class='escull-hores-taula-ocupat'>";
            }
            // Obtenemos el siguiente registro de la base de datos
            $row = mysqli_fetch_assoc($resultadoQuery);
        // Si no existe esta hora en la base de datos es porque puede estar o no disponible
        } else {
            // Estará ocupado si el día es hoy y la hora es menor a la actual
            if (strtotime($horaInicial) <= strtotime($horaActual) && strtotime($fechaActual) == strtotime($fechaEscogida)) {
                $htmlstrHoras.= "<td class='escull-hores-taula-ocupat'>";
                // Si no es que está libre
            } else {
                $htmlstrHoras.= "<td>";
            }
        }
        // Mostramos la hora en la tabla
        $htmlstrHoras.= "<input type='submit' name='hora-escollida' value='" . $horaInicial . "'>";
        $htmlstrHoras.= "</td>";
        // Al crear el nuevo item td hemos de incrementar el contador para ver si es el segundo item de la tabla
        $i++;
        // Si es el segundo item cerramos la fila
        if ($i % 2 == 0) {
            $htmlstrHoras.= "</tr>";
        }
        // Obtenemos la siguiente hora para comparar
        $horaInicial = date("H:i", strtotime(date("H:i", strtotime($horaInicial)) . " +30 minute"));
    }
    // Cerramos el cuerpo de la tabla
    $htmlstrHoras.= "</tbody>";
    // Cerramos la tabla
    $htmlstrHoras.= "</table>";
    // Liberamos la memoria
    mysqli_free_result($resultadoQuery);
}
// Controlamos el click si escoge una hora en la tabla
if (isset($_POST['hora-escollida'])) {
    // Enviamos y mostramos la siguiente ventana con los datos de la hora escogidos
    $horaEscogida = $_POST['hora-escollida'];
    $arrDatos = array(
        $fechaEscogida,
        $horaEscogida
    );
    $_SESSION['datosCitaPreviaDate'] = $arrDatos;
    header('location:../cita-previa-form/bodyForm.php');
    die();
}
/**
 * Funció que farem servir per obtindre el número de cites que hi ha registrades a una hora i dia
 */
function queryObtenerCitasFecha() {
    global $db_server, $db_database, $db_table, $campoDataCita, $fechaEscogida, $horaActual;
    // Abrimos la conexión con la base de datos
    obrirConexioDB();
    // Nos conectamos a la base de datos
    if (mysqli_select_db($db_server, $db_database)) {
        // Creamos la query para obtener las citas
        $queryCitasFecha="SELECT DATE_FORMAT(`$campoDataCita`, '%H:%i') AS keyHora, COUNT(*) AS numeroCitas FROM `$db_database`.`$db_table`
                                         WHERE `$campoDataCita` >=  '$fechaEscogida 08:00:00' AND  `$campoDataCita` <=  '$fechaEscogida 20:00:00'
                                         GROUP BY $campoDataCita";
        
        // Realizar consulta a la base de datos
        $resultadoQuery = mysqli_query($db_server, $queryCitasFecha);
        // Nos desconectamos de la base de datos
        tancarConexioDB();
        // Retornamos el número de filas totales como resultado
        return $resultadoQuery;
    } else {
        die("No s'ha pogut establir la conexió. " . mysqli_error($db_server));
    }
}
?>