<?php
	// Cargamos el php necesario para los datos
	require_once('php/Hores.php');
	// Cargamos los estilos necesarios para la web de bodyCitaPreviaDate.php
	$htmlstrhead = '<link rel="stylesheet" href="css/styleCitaPreviaDate.css">'
							 	 .'<script src="../js/jquery.js"></script>'
							 	 .'<script src="../js/js-cookie.js"></script>'
							 	 .'<script src="js/jsCitaPreviaDate.js"></script>';
	// Cargamos el código en el body de la web de bodyCitaPreviaDate.php
	$htmlstrbdy = "<form id='formulari-cita-previa-date' method='POST' action='bodyCitaPreviaDate.php'>"
									."<section class='seccio-contingut'>"
										."<section class='seccio-esquerra'>"
											."<section class='informacio'>"
												."<p>Temps: <span>30 min aprox.</span></p>"
												."<p>Preu: <span>50 € aprox.</span></p>"
											."</section>"
											."<section class='escull-data'>"
												."<p>Data: <span>"
												.'<input type="date" class="data-escollida" name="data-escollida" id="data-escollida" step="1" min="' . $fechaActual . '" max="' . $fechaLimite . '" value="' . $fechaEscogida . '">'
												.'<span class="data-escollida-validador"></span>'
												."</span></p>"
											."</section>"
										."</section>"
										."<section class='seccio-dreta'>"
											."<section class='escull-hores'>"
													.$htmlstrHoras
											."</section>"
										."</section>"
								."</section>"
							."</form>";
	// Cargamos la plantilla de la web
	require_once('../Plantilla.php');
?>