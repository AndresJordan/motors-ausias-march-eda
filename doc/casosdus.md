## Explicació dels casos d'ús / històries d'usuari amb la gestió dels casos extrems (com a mínim) ##

-Introdueix la matrícula amb més lletres o números dels necessaris -> Dóna error, s'ha de tornar a introduir.

-Escull una data que no té hora de cita -> No pot, ha d'escollir una de les lliures

-Escull una hora que està plena -> No es posible.

-Introdueix l'e-mail incorrectament -> Dóna error, ha de tornar a introduir el correu.

-Introdueix el telèfon amb lletres o més números dels necessaris -> Dóna error, ha de tornar a introduir el telèfon.

-Intenta accedir directament a una url sense els passos anterior que es requereixen. Per exemple, anar directament al confirmar la cita sense haver passat per introduir matrícula -> Seleccionar cita -> omplir dades (form) -> Dóna pantalla d'error.

-Intentar accedir a la pàgina d'administració -> No pot fer-ho, requereix credencials 